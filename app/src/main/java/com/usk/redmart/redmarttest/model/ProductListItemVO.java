package com.usk.redmart.redmarttest.model;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public class ProductListItemVO {

    private String imageUrl;
    private String title;
    private String price;
    private String productId;

    public ProductListItemVO() {

    }
    public ProductListItemVO(String productId, String imageUrl, String title, String price) {
        this.productId = productId;
        this.imageUrl = imageUrl;
        this.title = title;
        this.price = price;
    }
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return "$"+price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
