package com.usk.redmart.redmarttest.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.usk.redmart.redmarttest.PDPActivity;
import com.usk.redmart.redmarttest.R;
import com.usk.redmart.redmarttest.adapters.RecyclerAdapter;
import com.usk.redmart.redmarttest.constants.Constants;
import com.usk.redmart.redmarttest.constants.ServicesConstants;
import com.usk.redmart.redmarttest.custom_listeners.EndlessRecyclerViewScrollListener;
import com.usk.redmart.redmarttest.interfaces.FetchProductListListener;
import com.usk.redmart.redmarttest.interfaces.RecyclerItemClickListener;
import com.usk.redmart.redmarttest.model.ProductListItemVO;
import com.usk.redmart.redmarttest.model.ProductListVO;
import com.usk.redmart.redmarttest.web_services.ServiceController;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by UdaySravanK on 6/17/17.
 */

public class ProductListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, FetchProductListListener, RecyclerItemClickListener{

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerAdapter adapter;
    private ProductListVO listVO;
    private Context mContext;
    public static ProductListFragment newInstance() {
        return new ProductListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mContext = inflater.getContext();
        listVO = new ProductListVO();

        ServiceController.getInstance().setContext(mContext);
        ServiceController.getInstance().setFetchProductListListener(this);

        View root = inflater.inflate(R.layout.product_list_frag, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.product_list_view);
        adapter = new RecyclerAdapter(container.getContext());
        adapter.setRecyclerItemClickListener(this);
        adapter.setData(listVO);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager){
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if(listVO.getCurrentPage() == listVO.getLastPage()){
                    Toast.makeText(getContext(),
                            "Reached end of products list." ,
                            Toast.LENGTH_LONG).show();
                } else {
                    ServiceController.getInstance().fetchProducts(listVO.getCurrentPage()+1);
                }
            }
        });
        swipeRefreshLayout.setRefreshing(true);
        ServiceController.getInstance().fetchProducts(1);
        return root;
    }

    @Override
    public void onRefresh() {
        Toast.makeText(getContext(),
                "Refreshing products. Please wait." ,
                Toast.LENGTH_LONG).show();
        ServiceController.getInstance().fetchProducts(1);
    }

    public Context getContext(){
        return mContext;
    }

    @Override
    public void onPorductListFetchSuccess(JSONObject response,int page) {
        if(listVO.parseResponse(response, page)){
            adapter.setData(listVO);
            adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(getContext(),
                    "Unable to fetch products" ,
                    Toast.LENGTH_LONG).show();
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPorductListFetchError() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClick(ProductListItemVO itemVO) {
        Intent intent = new Intent(getContext(), PDPActivity.class);
        intent.putExtra(Constants.PRODUCT_ID_INTENT_KEY, itemVO.getProductId());
        startActivity(intent);
    }
}
