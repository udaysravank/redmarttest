package com.usk.redmart.redmarttest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.usk.redmart.redmarttest.constants.Constants;
import com.usk.redmart.redmarttest.interfaces.FetchProductDataListener;
import com.usk.redmart.redmarttest.model.ProductVO;
import com.usk.redmart.redmarttest.web_services.ServiceController;

import org.json.JSONObject;

public class PDPActivity extends AppCompatActivity implements FetchProductDataListener{

    private ImageView imageView;
    private TextView titleTextView;
    private TextView priceTextView;
    private TextView descTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdp);
        imageView =(ImageView) findViewById(R.id.productImageView);
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        priceTextView = (TextView) findViewById(R.id.priceTextView);
        descTextView = (TextView) findViewById(R.id.descTextView);
        String tcin = getIntent().getStringExtra(Constants.PRODUCT_ID_INTENT_KEY);
        ServiceController.getInstance().setFetchProductDataListener(this);
        ServiceController.getInstance().fetchProductData(tcin);
    }

    @Override
    public void onProductDataFetchSuccess(JSONObject response) {
        ProductVO productVO = new ProductVO(response);
        Picasso.with(this).load(productVO.getImageURL()).into(this.imageView);
        this.titleTextView.setText(productVO.getTitle());
        this.priceTextView.setText(productVO.getPrice());
        this.descTextView.setText(productVO.getDescription());
    }

    @Override
    public void onProductDataFetchError() {
        Toast.makeText(this,
                "Unable to fetch product details." ,
                Toast.LENGTH_LONG).show();
    }

}
