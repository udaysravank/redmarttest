package com.usk.redmart.redmarttest.interfaces;

import org.json.JSONObject;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public interface FetchProductListListener {

    void onPorductListFetchSuccess(JSONObject response, int page);

    void onPorductListFetchError();

}
