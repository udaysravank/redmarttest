package com.usk.redmart.redmarttest.interfaces;

import com.usk.redmart.redmarttest.model.ProductListItemVO;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public interface RecyclerItemClickListener {
    public void onItemClick(ProductListItemVO itemVO);
}
