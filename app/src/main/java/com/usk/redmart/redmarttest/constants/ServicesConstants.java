package com.usk.redmart.redmarttest.constants;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public final class ServicesConstants {
    public static final String PRODUCT_LIST_URL = "https://api.redmart.com/v1.5.7/catalog/search";
    public static final int PAGE_SIZE = 20;
    public static final String IMAGE_SERVER_BASE_URL = "http://media.redmart.com/newmedia/200p";
    public static final String PRODUCT_URL = "https://api.redmart.com/v1.5.7/catalog/products/";
}
