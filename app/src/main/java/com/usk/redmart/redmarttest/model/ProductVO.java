package com.usk.redmart.redmarttest.model;

import com.usk.redmart.redmarttest.constants.ServicesConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public class ProductVO {
    private String imageURL;
    private String title;
    private String price;
    private String description;

    public ProductVO() {

    }

    public ProductVO(JSONObject response) {
        this.parseResponse(response);
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONObject productObj = (JSONObject) response.getJSONObject("product");
            this.imageURL = productObj.getJSONObject("img").getString("name");
            this.title = productObj.getString("title");
            this.price = productObj.getJSONObject("pricing").getString("price");
            this.description = productObj.getString("desc");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getImageURL() {
        return ServicesConstants.IMAGE_SERVER_BASE_URL+imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return "$"+price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
