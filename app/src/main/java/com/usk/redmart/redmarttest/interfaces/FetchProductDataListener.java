package com.usk.redmart.redmarttest.interfaces;

import org.json.JSONObject;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public interface FetchProductDataListener {
    public void onProductDataFetchSuccess(JSONObject response);
    public void onProductDataFetchError();
}
