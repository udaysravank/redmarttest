package com.usk.redmart.redmarttest.model;

import com.usk.redmart.redmarttest.constants.ServicesConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public class ProductListVO {
    private int mCurrentPage = 1;
    private int lastPage = -1;

    private  ArrayList<ProductListItemVO> list = new ArrayList<ProductListItemVO>();
    public int size() {
        return list.size();
    }
    public ProductListItemVO get(int position) {
        return list.get(position);
    }
    public int getCurrentPage() {
        return mCurrentPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public boolean parseResponse(JSONObject response, int page) {
        if(page == 1) {
            list.clear();
        }
        try {
            JSONArray products = response.getJSONArray("products");
            mCurrentPage = page;
            if(products.length() < ServicesConstants.PAGE_SIZE) {
                // No of products in page is less than 20. Hence this is last page.
                lastPage = page;
                mCurrentPage = page;
            } else if(products.length() == 0) {
                // This page has zero products. Hence previous page is last page.
                mCurrentPage = page - 1;
                lastPage = page - 1;
            }
            for(int i=0;i<products.length();i++){
                JSONObject product = (JSONObject) products.get(i);
                String imageUri = ServicesConstants.IMAGE_SERVER_BASE_URL + product.getJSONObject("img").getString("name");
                String title = product.getString("title");
                String price = product.getJSONObject("pricing").getString("price");
                String productId = product.getString("id");
                ProductListItemVO itemVo = new ProductListItemVO(productId, imageUri, title, price);
                list.add(itemVo);
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

}
