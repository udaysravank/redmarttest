package com.usk.redmart.redmarttest.web_services;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.usk.redmart.redmarttest.constants.ServicesConstants;
import com.usk.redmart.redmarttest.interfaces.FetchProductDataListener;
import com.usk.redmart.redmarttest.interfaces.FetchProductListListener;

import org.json.JSONObject;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public class ServiceController {
    public static final String TAG = ServiceController.class.getSimpleName();
    private static ServiceController mInstance;

    private Context mContext;
    private RequestQueue mRequestQueue;
    private FetchProductDataListener fetchProductDataListener;
    private FetchProductListListener fetchProductListListener;

    public void setFetchProductListListener(FetchProductListListener fetchProductListListener) {
        this.fetchProductListListener = fetchProductListListener;
    }

    public static synchronized ServiceController getInstance() {
        if (mInstance == null) {
            mInstance = new ServiceController();
        }
        return mInstance;
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public void setFetchProductDataListener(FetchProductDataListener fetchProductDataListener) {
        this.fetchProductDataListener = fetchProductDataListener;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }

    public void fetchProducts(int page) {
        final int currentPage = (page - 1) >= 0 ? page - 1 : 0;
        String url = ServicesConstants.PRODUCT_LIST_URL + "?page=" + currentPage + "&pageSize=" + ServicesConstants.PAGE_SIZE;
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if (fetchProductListListener != null) {
                            fetchProductListListener.onPorductListFetchSuccess(response, currentPage+1);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (fetchProductListListener != null) {
                            fetchProductListListener.onPorductListFetchError();
                        }
                    }
                });

        ServiceController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void fetchProductData(String tcin) {
        String url = ServicesConstants.PRODUCT_URL + tcin;
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if (fetchProductDataListener != null) {
                            fetchProductDataListener.onProductDataFetchSuccess(response);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (fetchProductDataListener != null) {
                            fetchProductDataListener.onProductDataFetchError();
                        }
                    }
                });

        ServiceController.getInstance().addToRequestQueue(jsObjRequest);
    }
}
