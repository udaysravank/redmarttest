package com.usk.redmart.redmarttest.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.usk.redmart.redmarttest.PDPActivity;
import com.usk.redmart.redmarttest.R;
import com.usk.redmart.redmarttest.constants.Constants;
import com.usk.redmart.redmarttest.interfaces.RecyclerItemClickListener;
import com.usk.redmart.redmarttest.model.ProductListItemVO;
import com.usk.redmart.redmarttest.model.ProductListVO;

import java.util.List;

/**
 * Created by UdaySravanK on 6/18/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private Context mContext;
    private ProductListVO listVO;

    private RecyclerItemClickListener recyclerItemClickListener;

    public RecyclerAdapter(Context context) {
        mContext = context;
    }

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.recyclerItemClickListener = recyclerItemClickListener;
    }

    public void setData(ProductListVO listVO) {
        this.listVO = listVO;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View productListItem = inflater.inflate(R.layout.product_list_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(productListItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductListItemVO data = this.listVO.get(position);
        holder.setItemVO(data);
    }

    @Override
    public int getItemCount() {
        return this.listVO.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView imageView;
        private TextView titleTextView;
        private TextView priceTextView;
        private ProductListItemVO itemVO;

        public ViewHolder(View itemView){
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);
            itemView.setOnClickListener(this);
        }

        public void setItemVO(ProductListItemVO itemVO) {
            this.itemVO = itemVO;
            Picasso.with(mContext).load(itemVO.getImageUrl()).into(imageView);
            priceTextView.setText(itemVO.getPrice());
            titleTextView.setText(itemVO.getTitle());
        }

        @Override
        public void onClick(View view) {
            recyclerItemClickListener.onItemClick(itemVO);
        }
    }
}
