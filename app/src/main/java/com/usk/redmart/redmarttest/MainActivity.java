package com.usk.redmart.redmarttest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.usk.redmart.redmarttest.fragments.ProductListFragment;
import com.usk.redmart.redmarttest.utils.ActivityUtils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ProductListFragment productListFragment =
                (ProductListFragment) getSupportFragmentManager().findFragmentById(R.id.pl_container);
        if (productListFragment == null) {
            productListFragment = ProductListFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), productListFragment, R.id.pl_container);
        }
    }
}
